module Main where

import IlliterateProgramming (processLine, structureProse)

import Data.Text qualified as T
import Path (parent)
import Path.IO (resolveFile')
import Text.Megaparsec (errorBundlePretty)
import Std

main :: IO ()
main =
  do
    prosePath <-
      getArgs
        >>= \case
          [p] -> resolveFile' p
          _ -> throwString "Please provide one file path argument."
    -- copyFile prosePath =<< (addExtension ".backup" prosePath)
    (=<<) (writeFileUtf8 prosePath) $
      -- IO Text
      fmap T.concat $
      -- IO [Text]
      (fmap. fmap) (`T.snoc` '\n') $
      -- IO [Text]
      fmap concat $
      -- IO [[Text]]
      ((=<<) . traverse) (processLine $ parent prosePath) $
      -- IO [LineStructured]
      (=<<) (either (throwString . errorBundlePretty) pure) $
      -- IO (Either (ParseErrorBundle Text Void) [LineStructured])
      fmap (structureProse prosePath) $
      -- IO Text
      readFileUtf8 $
      -- Path Abs File
      prosePath
