# illiterate programming

## usage

install `cabal-install` and GHC 8.10.7. execute `cabal run exes examples/pythagoras.tex`.

## example

[`Pythagoras.hs`](examples/Pythagoras.hs):

```haskell
{-# LANGUAGE FlexibleContexts #-}

module Pythagoras where

import Data.Char (isSpace)
import Std hiding (many)

-- { all
-- { type
vectorLength :: Double -> Double -> Double
-- } type
vectorLength x y = sqrt (x^2 + y^2)
-- } all
```

[`pythagoras.tex`](examples/pythagoras.tex):

```latex
Let us define the following function.

% { Pythagoras.hs type
% } Pythagoras.hs type

The square of the hypothenuse is the sum
of the two right-angled sides squared:
$$a^2 + b^2 = c^2$$

% { Pythagoras.hs all
% } Pythagoras.hs all
```

executing `illiterate-programming pythagoras.tex` replaces `pythagoras.tex`'s content with the following.

```latex
Let us define the following function.

% { Pythagoras.hs type
\begin{minted}{haskell}
vectorLength :: Double -> Double -> Double
\end{minted}
% } Pythagoras.hs type

The square of the hypothenuse is the sum
of the two right-angled sides squared:
$$a^2 + b^2 = c^2$$

% { Pythagoras.hs all
\begin{minted}{haskell}
vectorLength :: Double -> Double -> Double
vectorLength x y = sqrt (x^2 + y^2)
\end{minted}
% } Pythagoras.hs all
```

re-execute `illiterate-programming pythagoras.tex` to update [`pythagoras.tex`](examples/pythagoras.tex) to incorporate any refactorings since the last run of `illiterate-programming`.
