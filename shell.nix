with import
  (fetchTarball {
    name = "nixos-22.11-2022-11-26";
    url = "https://github.com/NixOS/nixpkgs/archive/899e7caf59d1954882a8e2dff45ccc0387c186f6.tar.gz";
    sha256 = "06vj1qw5626yhx5mqy2js0dzyc7zrs73ygxz26049f7cdviwcmkx";
    # name = "nixos-unstable-2023-02-13";
    # url = "https://github.com/NixOS/nixpkgs/archive/6ee3fba086216b77cecd92d0b253727e51bbad68.tar.gz";
    # sha256 = "1kcg44brqa33vv7hirg3x3fzskdc0v0rkg2l74mfgp5b4fj0956s";
  })
    {};
mkShell {
  packages =
    [
      haskell.compiler.ghc924
      cabal-install
      haskellPackages.cabal-fmt
      ghcid
      # haskell-language-server
    ];
}
