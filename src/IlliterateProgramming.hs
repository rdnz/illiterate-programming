module IlliterateProgramming where

import Data.Char (isSpace)
import Data.Text qualified as T
import Path
  ( Dir,
    File,
    Path,
    Rel,
    fromRelFile,
    parseRelFile,
    toFilePath,
    (</>),
  )
import Text.Megaparsec
  ( MonadParsec (eof, notFollowedBy, takeWhile1P, takeWhileP),
    ParseErrorBundle,
    Parsec,
    many,
    parseMaybe,
    runParser,
    skipMany,
    (<?>),
  )
import Text.Megaparsec.Char (char, string)
import Std hiding (many)

{-
to do

options
- root path
- default evaluation command + regular expression
  - https://hackage.haskell.org/package/text-icu/docs/Data-Text-ICU-Regex.html
  - https://hackage.haskell.org/package/pcre-light/docs/Text-Regex-PCRE-Light.html
  - https://hackage.haskell.org/package/regex-pcre/docs/Text-Regex-PCRE.html
- default first and last line
-}

environmentBegin :: Maybe Text -> [Text]
environmentBegin Nothing = ["\\begin{minted}{haskell}"]
environmentBegin (Just options) =
  ["\\begin{minted}[" <> options <> "]{haskell}"]
environmentEnd :: [Text]
environmentEnd = ["\\end{minted}"]
proseComment :: Text
proseComment = "%"
codeComment :: Text
codeComment = "--"
commandBegin :: Text
commandBegin = "{"
commandEnd :: Text
commandEnd = "}"

data LineStructured =
  ProseLine Text |
  IncludeCommand
    Identifier
    (Maybe Text) -- ^ minted highlited lines

data Identifier =
  Identifier
    FilePath -- ^ module file path
    Text -- ^ label

type Parser = Parsec Void Text

processLine :: Path b Dir -> LineStructured -> IO [Text]
processLine _ (ProseLine line) = pure [line]
processLine
  root
  (IncludeCommand identifier@(Identifier modulePath _) optionsMaybe)
  =
    fmap (embedInLaTeX identifier optionsMaybe) $
    -- IO [Text]
    (=<<)
      (\case
        codeBlock@([]) ->
          putStrLn
            ("empty code block for " <> identifierToQuotedString identifier)
          $>
          codeBlock
        codeBlock -> pure codeBlock
      ) $
    -- IO [Text]
    (=<<) (either throwString pure) $
    -- IO (Either String [Text])
    fmap (getCodeBlock identifier) $
    -- IO Text
    readFileUtf8 $
    -- Path b File
    root </> modulePath

getCodeBlock :: Identifier -> Text -> Either String [Text]
getCodeBlock identifier@(Identifier _ label) =
  (=<<)
    (\case
      (codeBlock, _rest@(_:_)) ->
        Right $ filter (not . isLabel) codeBlock
      (_codeBlock, _rest@[]) ->
        Left $
          "end label missing for " <> show (identifierToText identifier)
    ) .
  -- Either String ([Text], [Text])
  fmap (break $ isEndLabel label) .
  -- Either String [Text]
  fmap tail .
  -- Either String (NonEmpty Text)
  maybe
    (Left $ identifierToQuotedString identifier <> " not found")
    Right .
  -- Maybe (NonEmpty Text)
  nonEmpty .
  -- [Text]
  dropWhile (not . isBeginLabel label) .
  -- [Text]
  lines

embedInLaTeX :: Identifier -> Maybe Text -> [Text] -> [Text]
embedInLaTeX identifier optionsMaybe codeBlock =
  [
    proseComment <> " " <>
    commandBegin <> " " <>
    identifierToText identifier <>
    case optionsMaybe of
      Nothing -> ""
      Just options -> " " <> options
  ] <>
  environmentBegin optionsMaybe <>
  codeBlock <>
  environmentEnd <>
  [
    proseComment <> " " <>
    commandEnd <> " " <>
    identifierToText identifier
  ]

structureProse ::
  Path b File ->
  Text ->
  Either (ParseErrorBundle Text Void) [LineStructured]
structureProse prosePath =
  runParser (many lineStructured <* eof) (toFilePath prosePath)

lineStructured :: Parser LineStructured
lineStructured =
  (ProseLine <$> proseLine)
  <|>
  (
    do
      (identifier, options) <- includeCommandBegin
      skipMany proseLine
      includeCommandEnd identifier
      pure (IncludeCommand identifier options)
  )

proseLine :: Parser Text
proseLine =
  notFollowedBy
    (
      string proseComment *>
      spaceExceptNewline *>
      (string commandBegin <|> string commandEnd)
    ) *>
  takeWhileP (Just "prose line character") (!= '\n') <*
  char '\n'

includeCommandBegin :: Parser (Identifier, Maybe Text)
includeCommandBegin =
  string proseComment *>
  spaceExceptNewline *>
  string commandBegin *>
  spaceExceptNewline *>
  ((,)
    <$>
      (Identifier
        <$> (T.unpack <$> notSpace)
        <*> (spaceExceptNewline1 *> notSpace)
      )
    <*> optional (spaceExceptNewline1 *> notSpace)
  ) <*
  takeWhileP (Just "prose line character") (!= '\n') <*
  char '\n'

includeCommandEnd :: Identifier -> Parser ()
includeCommandEnd identifier =
  string proseComment *>
  char ' ' *>
  string commandEnd *>
  char ' ' *>
  string (identifierToText identifier) *>
  char '\n' *>
  pure ()
  <?>
  (
    "\"" <> T.unpack proseComment <> " " <>
    T.unpack commandEnd <> " " <>
    T.unpack (identifierToText identifier) <> "\""
  )

spaceExceptNewline :: Parser ()
spaceExceptNewline =
  void $ takeWhileP Nothing (\c -> isSpace c && c != '\n')

spaceExceptNewline1 :: Parser ()
spaceExceptNewline1 =
  void $
    takeWhile1P
      (Just "white space except for newline")
      (\c -> isSpace c && c != '\n')

notSpace :: Parser Text
notSpace = takeWhile1P (Just "not white space") (not . isSpace)

isBeginLabel :: Text -> Text -> Bool
isBeginLabel label line =
  line == codeComment <> " " <> commandBegin <> " " <> label

isEndLabel :: Text -> Text -> Bool
isEndLabel label line =
  line == codeComment <> " " <> commandEnd <> " " <> label

isLabel :: Text -> Bool
isLabel =
  isJust .
  parseMaybe
    (
      string codeComment *>
      char ' ' *>
      (string commandBegin <|> string commandEnd) *>
      char ' ' *>
      notSpace
    )

identifierToText :: Identifier -> Text
identifierToText (Identifier modulePath label) =
  T.pack (fromRelFile modulePath) <> " " <> label

identifierToQuotedString :: Identifier -> String
identifierToQuotedString = show . identifierToText
