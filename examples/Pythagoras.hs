{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Pythagoras where

import Data.Char (isSpace)
import Data.Text qualified as T
import Path
  ( Dir,
    File,
    Path,
    Rel,
    addExtension,
    fromRelFile,
    parent,
    parseRelFile,
    toFilePath,
    (</>),
  )
import Path.IO (copyFile, resolveFile')
import Text.Megaparsec
  ( MonadParsec (eof, notFollowedBy, takeWhile1P, takeWhileP),
    ParseErrorBundle,
    Parsec,
    errorBundlePretty,
    many,
    parseMaybe,
    runParser,
    skipMany,
    (<?>),
  )
import Text.Megaparsec.Char (char, string)
import Std hiding (many)

-- { all
-- { type
vectorLength :: Double -> Double -> Double
-- } type
vectorLength x y = sqrt (x^2 + y^2)
-- } all
